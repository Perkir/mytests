﻿using System;

namespace MoneyConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            Money moneyRUR = new Money(1500, CurrencyTypes.RUR);
            Money moneyUSD = new Money(5550,CurrencyTypes.USD);
            //Money money3 = new Money(2500,CurrencyTypes.EUR);
            
            Console.WriteLine("Сложим две валюты");
            Money convertMoney = moneyRUR + moneyUSD;
            Console.WriteLine($"Сумма {convertMoney.Amount}, тип {convertMoney.CurrencyType}");
            
            Console.WriteLine("Сложим валюту с числом");
            Money moneyWithInteger = moneyRUR + 1234.5;
            Console.WriteLine($"Сумма {moneyWithInteger.Amount}, тип {moneyWithInteger.CurrencyType}");
            
            Console.WriteLine("Уменьшим валюту на 1");
            moneyUSD--;
            Console.WriteLine($"Сумма {moneyUSD.Amount}, тип {moneyUSD.CurrencyType}");
            // increase 1st object of Money
            Console.WriteLine("Умножим валюту");
            moneyRUR *= 3;
            Console.WriteLine($"Сумма {moneyRUR.Amount}, тип {moneyRUR.CurrencyType}");

            Console.WriteLine("Превратим в строку");
            Console.WriteLine($"Результат: {moneyRUR.ToString()}");
            
            Console.WriteLine("Проверим работу метода расширения");
            var s = "1";
            Console.WriteLine("Было: " + s);
            Console.WriteLine("Стало: " + MoneyExtension.RurAmount(s));
            
            Console.WriteLine("Проверяем работу кольцевого списка");
            
            var list = new CycledLinkedList<int>();
            // Добавляем элементы.
            list.Add(1);
            list.Add(5);
            list.Add(17);
            list.Add(42);
            list.Add(-69);
 
            // Выводим все элементы на консоль.
            foreach(var item in list)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
 
            // Удаляем элемент.
            list.Delete(17);
 
            // Выводим все элементы еще раз.
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }

            var bs = list.BinarySearch(list, 1);
            
            Console.WriteLine(bs);
        }
    }
}