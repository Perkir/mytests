using System;

namespace MoneyConverter
{
    /// <summary>
    /// класс с методом расширения
    /// </summary>
    public static class MoneyExtension
    {
        public static string RurAmount(this string str)
        {
            return Convert.ToInt32(str + 100).ToString();
        }
    }
}