using System.Collections.Generic;
using System;
using System.Collections;
using System.Linq;

namespace MoneyConverter
{
    public class CycledLinkedList<T> : IEnumerable<T>, IAlgorithm
    {
        public Item<T> Head { get; set; }
        public Item<T> Tail { get; set; }
        public int Count { get; set; }
        
        public CycledLinkedList() {}
    
        public CycledLinkedList(T data)
        {
            var item = new Item<T>(data);
            Head = item;
            Tail = item;
            Count = 1;
        }

        public void Add(T data)
        {
            var item = new Item<T>(data);
            if(Head == null)
            {
                Head = item;
            }
            else
            {
                Tail.Next = item;
            }
            Tail = item;
            Count++;
        }

        public void Delete(T data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var current = Head;
            Item<T> previous = null;
            while (current != null)
            {
                if (current.Data.Equals(data))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;
                        if (current.Next == null)
                        {
                            Tail = previous;
                        }
                    }
                    else
                    {
                        Head = Head.Next;
                        if (Head == null)
                        {
                            Tail = null;
                        }
                    }

                    Count--;
                    break;
                }

                previous = current;
                current = current.Next;
            }
        }

        public void Clear()
        {
            Head = null;
            Tail = null;
            Count = 0;
        }


        public IEnumerator<T> GetEnumerator()
        {
            var current = Head;
            while(current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        public int BinarySearch<CycledLinkedList>(CycledLinkedList list, int searchedObject)
        {
            
            //while ()
            //{
                //return list.GetType()
            //}

            return 1; 
        }
    }
}