using System.Collections.Generic;

namespace MoneyConverter
{
    public interface IAlgorithm
    {
        int BinarySearch<T>(T list, int searchedObject);
    }
}