namespace MoneyConverter
{

    /// Класс Деньги
    /// Служит для реализации различных операций с несколькими валютами
    public class Money
    {
        /// создаем индексаторы
        public decimal Amount { get; set; }
        public CurrencyTypes CurrencyType { get; set; }

        /// создаем пустой констурктор
        public Money(){ }
        /// конструктор с параметрами
        public Money(decimal amount, CurrencyTypes type)
        {
            Amount = amount;
            CurrencyType = type;
        }
        /// перегрузим оператор сложения двух валют
        public static Money operator+(Money money1,Money money2)
        => new Money() { Amount = CalculateAmount(money1) + CalculateAmount(money2), CurrencyType = CurrencyTypes.RUR };
        /// перегрузим оператор для сложения валюты с числом
        public static Money operator +(Money money, double value)
        => new Money() { Amount = CalculateAmount(money) + (decimal)value, CurrencyType = CurrencyTypes.RUR };
        /// перегрузим оператор умножения валюты
        public static Money operator *(Money money, int count)
        {
            money.Amount *= count;
            return money;
        }
        /// перегрузим оператор для вычитания валют
        public static Money operator--(Money money)
        {
            money.Amount--;
            return money;
        }
        
        /// создадим метод для расчета валюты
        public static decimal CalculateAmount(Money money)
        {
            switch (money.CurrencyType)
            {
                case CurrencyTypes.RUR:
                    return money.Amount;
                case CurrencyTypes.USD:
                    return money.Amount * 64;
                case CurrencyTypes.EUR:
                    return money.Amount * (decimal) 70.42;
            }

            return 0;
        }

        public override string ToString() => $"{Amount} {CurrencyType}";
    }
}