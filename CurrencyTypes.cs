namespace MoneyConverter
{
    /// список возможных валют
    public enum CurrencyTypes
    {
        RUR,
        USD,
        EUR
    }
}